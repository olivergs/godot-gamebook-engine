# Context
%s

# Plugin helpers
%s

# Section related helpers
func total_visited():
	return _visited_sections.size()

func visited(section):
	return section in _visited_sections

func first_time_here():
	return _visited_sections.back() == _current_section \
		and _visited_sections.count(_current_section) == 1

# Internal expression evaluation method
func _eval():
	return %s