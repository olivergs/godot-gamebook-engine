extends Node

var _state = {}
var engine = null

func init(instance):
	engine = instance

func get_eval_helpers():
	return ""

func get_eval_context():
	return {}

func get_state():
	return _state

func set_state(data):
	_state = data

func reset_state():
	_state = {}

func process_macro(macro, section):
	pass