extends Node

# Inventory plugin


const DEFAULT_STATE = {
	"inventory": {},
	"dropped": {}
}

# Errors
const ERR_ADD_ANOTHER_UNIQUE_ITEM = 1001
const ERR_REMOVING_UNAVAILABLE_ITEMS = 1002
const ERR_ITEM_NOT_FOUND = 1003

var _error_messages = {
	ERR_ADD_ANOTHER_UNIQUE_ITEM: "Can not add another %s unique item",
	ERR_REMOVING_UNAVAILABLE_ITEMS: "Trying to remove %s %s items but only %s are available",
	ERR_ITEM_NOT_FOUND: "Item %s is not in inventory",
}

var _state = DEFAULT_STATE

var engine = null

const _eval_helpers = """
func are_there_dropped_items():
	if _current_section in plugins["inventory"]["dropped"]:
		return plugins["inventory"]["dropped"][_current_section].size() > 0
	return false

func get_inventory():
	return plugins["inventory"]["inventory"].keys()

func has_item(item):
	return item in get_inventory()

func item_has_tag(item, tag):
	if has_item(item):
		return tag in plugins["inventory"]["inventory"][item]["tags"]
	return false

func item_count(item):
	if has_item(item):
		return plugins["inventory"]["inventory"][item]["count"]
	return 0

func item_tags(item):
	if has_item(item):
		return plugins["inventory"]["inventory"][item]["tags"]
	return 0

func get_items_by_tag(tag):
	var items = []
	for item in get_inventory():
		if item_has_tag(item, tag):
			items.append(item)
	return items
"""

# Macro regexps
var _macro_regexps = {
	"add_item": Utils.setup_regex("^%add_item (?<item>\\w+)(?<count> \\d+)*(?<tags> .*)*$"),
	"remove_item": Utils.setup_regex("^%remove_item (?<item>\\w+)(?<count> \\d+)*$"),
	"tag_item": Utils.setup_regex("^%tag_item (?<item>\\w+)(?<tags> .*)*$"),
	"untag_item": Utils.setup_regex("^%untag_item (?<item>\\w+)(?<tags> .*)*$"),
	"remove_all_items": Utils.setup_regex("^%remove_all_items (?<item>\\w+)$"),
	"remove_items_by_tag": Utils.setup_regex("^%remove_items_by_tag (?<tags>.+)*$"),
	"drop_item": Utils.setup_regex("^%drop_item (?<item>\\w+)(?<count> \\d+)*$"),
	"take_item": Utils.setup_regex("^%take_item (?<item>\\w+)(?<count> \\d+)*$")
}

func init(instance):
	engine = instance


func get_eval_helpers():
	return _eval_helpers

func get_eval_context():
	return _state

func get_state():
	return _state

func set_state(data):
	_state = data

func reset_state():
	_state = DEFAULT_STATE

func process_macro(macro, section, metadata):
	if macro.begins_with("%add_item"):
		var result = _macro_regexps["add_item"].search(macro)
		if result:
			var item = result.get_string(1)
			var count = result.get_string(2).to_int()
			if count == 0:
				count = 1
			var tags = result.get_string(3).strip_edges()
			if tags:
				tags = Array(tags.split(" "))
			else:
				tags = []
			add_item(item, count, tags)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%tag_item"):
		var result = _macro_regexps["tag_item"].search(macro)
		if result:
			var item = result.get_string(1)
			var tags = result.get_string(2).strip_edges()
			if tags:
				tags = tags.split(" ")
			else:
				tags = []
			tag_item(item, tags)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%untag_item"):
		var result = _macro_regexps["untag_item"].search(macro)
		if result:
			var item = result.get_string(1)
			var tags = result.get_string(2).strip_edges()
			if tags:
				tags = tags.split(" ")
			else:
				tags = []
			untag_item(item, tags)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%remove_all_items"):
		var result = _macro_regexps["remove_all_items"].search(macro)
		if result:
			var item = result.get_string(1)
			remove_all_items(item)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%remove_items_by_tag"):
		var result = _macro_regexps["remove_items_by_tag"].search(macro)
		if result:
			var tags = result.get_string(1).strip_edges()
			tags = tags.split(" ")
			remove_items_by_tag(tags)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%remove_item"):
		var result = _macro_regexps["remove_item"].search(macro)
		if result:
			var item = result.get_string(1)
			var count = result.get_string(2).to_int()
			if count == 0:
				count = 1
			remove_item(item, count)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%drop_item"):
		var result = _macro_regexps["drop_item"].search(macro)
		if result:
			var item = result.get_string(1)
			var count = result.get_string(2).to_int()
			if count == 0:
				count = 1
			drop_item(engine.get_current_section(), item, count)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%take_item"):
		var result = _macro_regexps["take_item"].search(macro)
		if result:
			var item = result.get_string(1)
			var count = result.get_string(2).to_int()
			if count == 0:
				count = 1
			take_item(engine.get_current_section(), item, count)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	else:
		return false
	return true

#----------------------------------------------------------------------
# Plugin methods
#----------------------------------------------------------------------

func get_inventory():
	"""
	Returns all the objects in the inventory
	"""
	return _state["inventory"].keys()

func has_item(item):
	"""
	Check if item is in inventory
	"""
	return item in _state["inventory"].keys()

func get_item(item):
	"""
	Returns an object in the inventory with its count and tags
	"""
	if has_item(item):
		return _state["inventory"][item]
	else:
		engine.emit_error_signal(ERR_ITEM_NOT_FOUND,
			item,
			_error_messages[ERR_ITEM_NOT_FOUND])

func item_count(item):
	if has_item(item):
		return _state["inventory"][item]["count"]
	else:
		return 0

func add_item(item, count=1, tags=[]):
	"""
	Add an item with given tags
	"""
	if not has_item(item):
		_state["inventory"][item] = {
			"count": count,
			"tags": tags
		}
		engine.emit_plugin_signal("added_inventory_item", [item, count, tags])
		engine.emit_plugin_signal("inventory_updated", [])
	else:
		if not item_has_tag(item, "unique"):
			_state["inventory"][item]["count"] += count
			tag_item(item, tags)
			engine.emit_plugin_signal("added_inventory_item", [item, count, tags])
			engine.emit_plugin_signal("inventory_updated", [])
		else:
			# Emit error signal
			engine.emit_error_signal(ERR_ADD_ANOTHER_UNIQUE_ITEM,
				item,
				_error_messages[ERR_ADD_ANOTHER_UNIQUE_ITEM])

func remove_all_items(item):
	"""
	Removes all items for given item identifier
	"""
	if has_item(item):
		var count = item_count(item)
		_state["inventory"].erase(item)
		engine.emit_plugin_signal("removed_inventory_item", [item, count])
		engine.emit_plugin_signal("inventory_updated", [])
	else:
		engine.emit_error_signal(ERR_ITEM_NOT_FOUND,
			item,
			_error_messages[ERR_ITEM_NOT_FOUND])

func remove_items_by_tag(tags):
	"""
	Removes all items tagged with any of the specified tags
	"""
	var items = get_items_by_tags(tags)
	for item in items:
		remove_all_items(item)

func get_items_by_tag(tag):
	"""
	Returns a list of items tagger with specified tag
	"""
	var items = []
	for item in _state["inventory"].keys():
		if item_has_tag(item, tag):
			items.append(item)
	return items

func get_items_by_tags(tags):
	"""
	Returns a list of items tagged as any of the specified tags
	"""
	var items = []
	for tag in tags:
		for item in _state["inventory"].keys():
			if not item in items:
				if item_has_tag(item, tag):
					items.append(item)
	return items

func remove_item(item, count=1):
	"""
	Remove items from inventory
	"""
	if has_item(item):
		var icount = item_count(item)
		if count == icount:
			_state["inventory"].erase(item)
		elif count < icount:
			_state["inventory"][item]["count"] -= count
		else:
			engine.emit_error_signal(ERR_REMOVING_UNAVAILABLE_ITEMS,
				[count, item, icount],
				_error_messages[ERR_REMOVING_UNAVAILABLE_ITEMS])
			return
		engine.emit_plugin_signal("removed_inventory_item", [item, count])
		engine.emit_plugin_signal("inventory_updated", [])
	else:
		engine.emit_error_signal(ERR_ITEM_NOT_FOUND,
			item,
			_error_messages[ERR_ITEM_NOT_FOUND])

func item_tags(item):
	"""
	Returns the tags for an item
	"""
	if has_item(item):
		return _state["inventory"][item]["tags"]
	else:
		engine.emit_error_signal(ERR_ITEM_NOT_FOUND,
			item,
			_error_messages[ERR_ITEM_NOT_FOUND])

func item_has_tag(item, tag):
	"""
	Check an item has a tag
	"""
	if has_item(item):
		return tag in _state["inventory"][item]["tags"]
	else:
		engine.emit_error_signal(ERR_ITEM_NOT_FOUND,
			item,
			_error_messages[ERR_ITEM_NOT_FOUND])

func tag_item(item, tags):
	"""
	Add tags to an item
	"""
	if has_item(item):
		for tag in tags:
			if not item_has_tag(item, tag):
				_state["inventory"][item]["tags"].append(tag)
	else:
		engine.emit_error_signal(ERR_ITEM_NOT_FOUND,
			item,
			_error_messages[ERR_ITEM_NOT_FOUND])

func untag_item(item, tags):
	"""
	Remove tags from an item
	"""
	if has_item(item):
		for tag in tags:
			if item_has_tag(item, tag):
				_state["inventory"][item]["tags"].erase(tag)
	else:
		engine.emit_error_signal(ERR_ITEM_NOT_FOUND,
			item,
			_error_messages[ERR_ITEM_NOT_FOUND])


# Dropped items methods

func get_dropped_items(section):
	if section in _state["dropped"]:
		return _state["dropped"][section].keys()
	return []


func get_dropped_item(section, item):
	var dropped = _state["dropped"]
	if section in dropped:
		if item in dropped[section]:
			return dropped[section][item]
	return null


func drop_item(section, item, count=1):
	if has_item(item):
		var current_count = item_count(item)
		if  current_count < count:
			count = current_count
		var dropped = _state["dropped"]
		var obj = get_item(item)
		if not section in dropped:
			dropped[section] = {}
		if not item in dropped[section]:
			dropped[section][item] = {
				"count": count,
				"tags": obj["tags"],
			}
		else:
			dropped[section][item]["count"] += obj["count"]
			for tag in obj["tags"]:
				if not tag in dropped[section][item]["tags"]:
					dropped[section][item]["tags"].append(tag)
		# Remove item from inventory
		remove_item(item, count)


func take_item(section, item, count=1):
	print("TAKING", _state)
	var obj = get_dropped_item(section, item)
	if obj != null:
		if count < obj["count"]:
			obj["count"] -= count
			add_item(item, count, obj["tags"])
		else:
			add_item(item, obj["count"], obj["tags"])
			_state["dropped"].erase(item)
	print("TAKEN", _state)