## Inventory plugin

This plugin allows to manage the inventory during an adventure.

The items are identifiers that can be linked to more complex representations at
game representation level.

Every item added has a count that is increased or decreased when adding or
removing items

Items can have tags that define special behaviors that can be used to check
things like: Is a light source, is magical, can be used as weapon, is broken
or whatever. It is up to the developer how to use this tagging system.

There are some special tags that are taken in account for the plugin behavior:

* **unique**: This item is unique, so if we try to add a second item the item
will not be added.


### Eval context

The eval context for this plugin is a dictionary with item identifiers as keys
and dictionaries containing the keys count and tags:

    {
        "diamond": {
            "count": 1,
            "tags": ["gem", "unique"]
        },
        "stone": {
            "count": 4,
            "tags": ["heavy"]
        }
    }


### Eval helpers

* **has_item(item)**: Returns true if the item is in the inventory
* **item_has_tag(item, tag)**: Returns true if an item has the specified tag
* **item_count(item)**: Returns current amount of given item
* **item_tags(item)**: Returns the list of tags for an item
* **get_items_by_tag(tag)**: Returns a list of items tagged with the specified tag
* **get_inventory()**: Returns a list of of items in the inventory

### Macros

* **add_item item [*count tag1 tag2 ...*]**: Adds item to the inventory with
given count and tags
* **remove_item item [*count*]**: Remove an item. Can remove several items in
one step
* **%tag_item item tag1 [*tag2 ...*]**: Add given tags to an item
* **%untag_item item tag1 [*tag2 ...*]**: Remove the specified tags from an item
* **remove_all_items item**: Removes all items for given item identifier
* **remove_items_by_tag tag1 [*tag2 ...*]**: Remove the items tagged with given tag or tags

### Plugin methods

* **get_inventory()**: Returns all the objects in the inventory
* **has_item(item)**: Returns true if the item is in the inventory
* **get_item(item)**: Returns the specified item
* **item_count(item)**: Returns the count for the given item
* **add_item(item, count=1, tags=[])**: Add an item with given count and tags
* **remove_item(item, count=1)**: Remove an item from inventory
* **tag_item(item, tags)**: Add tags to an item
* **untag_item(item, tags)**: Remove tags from an item
* **item_has_tag(item, tag)**: Returns true if the item has the specified tag
* **remove_all_items(item)**: Removes all items for given item identifier
* **remove_items_by_tag(tags)**: Remove the items tagged with given tag or tags
* **get_items_by_tag(tag)**: Returns a list of the items tagged with given tag
* **get_items_by_tags(tags)**: Returns a list of items tagged as any of the specified tags


### Plugin signals

* **added_inventory_item [item, count, tags]**: Item has been added to inventory
* **removed_inventory_item [item, count]**: Item has been removed from inventory
* **inventory_updated []**: Inventory has been modified
