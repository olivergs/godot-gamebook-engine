extends Node

# Maps plugin

# Errors
const ERR_UNKNOWN_MAP = 2001

var _error_messages = {
	ERR_UNKNOWN_MAP: "Unknown specified map",
}


const DEFAULT_STATE = {
	"maps": {},
	"current_map": null,
	"current_loc": null
}


var _state = DEFAULT_STATE
var engine = null


const _eval_helpers = """
func get_maps():
	return plugins["maps"]["maps"].keys()


func get_map_locations(map):
	var state = plugins["maps"]
	if map in state["maps"]:
		return state["maps"][map]
	else:
		return []


func is_location_known(map, location):
	return location in get_map_locations(map)
"""


var _macro_regexps = {
	"set_current_location": RegEx.new()
}


func init(instance):
	engine = instance
	# Initialize regular expressions for macros
	_macro_regexps["set_current_location"].compile("^%set_current_location (?<map>\\w+) (?<location>\\w+)$")


func get_eval_helpers():
	return _eval_helpers


func get_eval_context():
	return _state


func get_state():
	return _state


func set_state(data):
	_state = data


func reset_state():
	_state = DEFAULT_STATE


func process_macro(macro, section, metadata):
	if macro.begins_with("%set_current_location"):
		var result = _macro_regexps["set_current_location"].search(macro)
		if result:
			var map = result.get_string(1)
			var location = result.get_string(2)
			set_current_location(map, location)
		else:
			engine.emit_error_signal(engine.ERR_INVALID_SYNTAX, macro)
	else:
		return false
	return true


#----------------------------------------------------------------------
# Plugin methods
#----------------------------------------------------------------------

func add_map(map):
	if not map in _state["maps"]:
		_state["maps"][map] = []
		engine.emit_plugin_signal("added_map", [map])


func add_location_to_map(map, location):
	add_map(map)
	if not location in _state["maps"][map]:
		_state["maps"][map].append(location)
		engine.emit_plugin_signal("added_map_location", [map, location])


func set_current_location(map, location):
	add_map(map)
	var old_map = _state["current_map"]
	_state["current_map"] = map
	if location != null:
		add_location_to_map(map, location)
	var old_location = _state["current_loc"]
	_state["current_loc"] = location
	if map != old_map or location != old_location:
		engine.emit_plugin_signal("map_location_changed", [map, location])
	engine.emit_plugin_signal("map_location_set", [map, location])


func get_current_map():
	return _state["current_map"]


func get_current_location():
	return _state["current_loc"]


func get_maps():
	return _state["maps"].keys()


func get_map_locations(map):
	if map in _state["maps"]:
		return _state["maps"][map]
	else:
		engine.emit_error_signal(
			ERR_UNKNOWN_MAP,
			map,
			_error_messages[ERR_UNKNOWN_MAP])


func is_location_known(map, location):
	return location in get_map_locations()
