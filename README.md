## Gamebook language specification

All the evaluations in the language are done in a GDScript sandbox with a context set with variables and builtin utilities. All GDScript can be used there.

### Language

```
@section_label* {"map": "world_map_name", "coords": [100, 100]}

# This is a comment. The label ending with an * means this is the starting one

This is normal text paragraph

?(expression) This is another paragraph only shown if expression is true

.stylename This is a text with "stylename" style applied

-character This is text spoken by an specific character

=variable_to_set expression_for_value

>@label_to_jump_to

This is a text with an inline ${expression} that will evaluate to a string

# Macros are actions that can be executed during the story
%macro parameters

!image_name_to_show

\[\[Normal option|@label_to_jump_to\]\]
?(expression) \[[Conditional option only shown when expression is true|@other_label\]\]
\[\[Another option|@another_label\]\]
\[[An option that will load the story from another module|module_file_name.gbe:@external_label\]\]
```

#### Basic text

Basic text is written as is. Just write the paragraph you want to show to the user

#### Section labels

Section labels starts with @ and specifies the start of a section. This labels can be used to jump to them.

If a section label has an * after its name, this section is considered to be the starting section for the story.

At each section we can specify metadata in JSON format for things like specifying map positions, ambient music, background images, theme coloring, screen effects... Metadata is avaliable at evaluating expressions under \_current_section_metadata variable and when a section is processed, it is passed to the section_processed signal. The management of metadata information are delegated in the viewer software.

We can also specify metadata by using the "*%metadata*" macro.

#### Comments

Comments start with an \#

#### Inline expressions

Their syntax is ${expression} and when they are evaluated they are converted to strings and replaced in the text by their value. The default value when an expression is empty or null, is NONE.

## Helper functions

This are functions that can be used in the expressions to check conditions or to get specific values

```
# Get total number of visited sections
total_visited()

# Know if an specific section has been visited before
visited(section_name)

# Know if current section has been visited before
first_time_here()
```

## Built-in macros

* **%metadata**: Allows specifying metadata for current section usin JSON format
* **%load_plugin name**: Loads a plugin to be used in the story
* **%set_var name value**: Sets a variable with given value
* **%jump section**: Jumps to given section and process it
* **%media identifier parms**: Fires a media event for given media and parameters
* **%END**: The story has ended. This will fire the story_ended signal

## Engine methods
* **get_history()**: Get section history
* **get_current_state()**: Get current state
* **get_current_module()**: Get current module name
* **get_current_section()**: Get current section name
* **get_current_line()**: Get current line

## Engine signals

Signals are fired to handle events on the engine

### Line processing signals

Line processing signals are fired when a line is processed. The signals receive
the relevant line information so the interface can represent it accordingly to
their type.

* **processed_section(section, metadata, interrupted)**: Processed a section.
	Metadata is a dictionary with data specified in section line at story file.
	Intrerrupted indicates if this section has been interrupted when finished
	the processing.	For example, when a jump instruction is processed, current
	section processing is interrupted.
* **processed_text(text)**: Processed text line
* **processed_option(text, dest, module)**: Processed an option line
* **processed_jump(dest)**: Processed a jump to another section
* **processed_conditional(cond, eval, next)**: Processed a conditional line
* **processed_character_speech(charname, text)**: Processed a character speech
	line.
* **processed_styled_text(style, text)**: Processed a styled text line
* **processed_media(media, parms)**: Processed a media line. Parms are handled
	by the application at developer's discretion.

### Error signals

Error signals are fired when an error occurs to be able to handle it.

* **caught_error(code, desc)**: Current line has thrown an error

### Special signals

* **processed_ending()**: Got an ending for the story
* **got_plugin_signal(name, parms)**: Got a custom plugin signal with given name and parameters. The parameters are a list with parameters inside it.

### Module loading signals

* **loaded_file(path)**: Not implemented yet. Is fired when a module has finished loading from a file

* **loaded_url(url)**: Not implemented yet. Is fired when a module has finished loading from an URL


## Plugins

Plugins can extend the engine functionality adding support to handle inventories, and complex tasks that are not built in in the engine.

### Registering plugins

All plugins need to be located at an folder at res://gbe_plugins/. That folder will be the name for the plugin.

Inside there should be a file called plugin.gd that has all the information needed to load the plugin.

Each plugin manages its own state

### Plugin class methods
* **init(engine)**: Called when plugin is loaded. The engine parameter is an instance of the engine loading the plugin.
* **get_eval_helpers()**: Returns the code for the eval helpers that will be included to be used during the story. If no helpers just returns an empty string.
* **get_eval_context()**: Returns data to be added to eval context so it can be used in expressions. If no eval context, just returns an empty dictionary.
* **get_state()**: Returns the state of this plugin. If the plugin does not manage any data, it just returns an empty dictionary. It is executed when saving the game state to save plugin state.
* **set_state(data)**: Sets the state of the plugin. It is executed when loaded data from a saved state.
* **reset_state()**: Resets the state of the plugin. Executed when general status is reset.
* **process_macro(macro, section)**: When a macro is found it is passed to each plugin to handle it. The macro will stop to be passed when one of the plugins returns true as the result of the execution of this.

### Plugin signals

Plugins can emit custom signals. The way to do this is to use the **emit_plugin_signal** method of the engine so a **got_plugin_signal** is emitted with given signal name and parameters.

## Twine format

### Story data element

	tw-storydata
		name="ANRD - Intro Detective Privado"
		startnode="11"
		creator="Twine"
		creator-version="2.2.1"
		ifid="AB3D911E-EA2E-4999-A741-A29AD20E090A"
		zoom="0.25"
		format="SugarCube"
		format-version="2.21.0"
		options=""
		hidden

### Passage data element

	tw-passagedata
		pid="11"
		name="@player_private_investigator_desc"
		tags=""
		position="457,158"
		size="100,100"
