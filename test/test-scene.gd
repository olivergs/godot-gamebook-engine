extends Panel

const optionkeys = [
	KEY_0, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8]

#var initial_module = "res://test/test.html"
var initial_module = "res://test/test.gbe"
var current_options = {}

onready var scroll = get_node("ScrollContainer")
onready var vbox = get_node("ScrollContainer/VBoxContainer")
var _need_scroll = false

func _ready():
	# Disable input until a section is processed
	set_process_input(false)

	# Connect signals
	GBE.connect("processed_section", self, "processed_section")
	GBE.connect("processed_text", self, "processed_text")
	GBE.connect("processed_option", self, "processed_option")
	GBE.connect("processed_jump", self, "processed_jump")
	GBE.connect("processed_ending", self, "processed_ending")
	GBE.connect("processed_conditional", self, "processed_conditional")
	GBE.connect("processed_char_speech", self, "processed_character_speech")
	GBE.connect("processed_styled_text", self, "processed_styled_text")
	GBE.connect("processed_media", self, "processed_media")
	GBE.connect("caught_error", self, "caught_error")
	GBE.connect("loaded_url", self, "loaded_url")

	# Load module and start the adventure
	GBE.load_file(initial_module)
	# Start adventure
	GBE.start()

#	# Continue story
#	GBE.save_state("res://test/test_state.sav")
#	GBE.load_state("res://test/test_state.sav")
#	GBE.continue()

func _process(delta):
	# Scrolling takes place after processing
	if _need_scroll:
		# Scroll to end
		var vscrollbar = scroll.get_children()[2]
		scroll.set_v_scroll(vscrollbar.get_max())
		_need_scroll = false

func add_item(text):
	var item = Label.new()
	vbox.add_child(item)
	item.show()
	item.set_autowrap(true)
	item.set_text(text)

	# Now we need scrolling
	_need_scroll = true
		
func caught_error(code, message):
	add_item("ERROR (Err: %s): %s" % [code, message])

func processed_section(section, interrupted):
	if interrupted:
		add_item("Finished processing section %s. Interrupted" % section)
	else:
		add_item("Finished processing section %s" % section)
		# Activate input
		set_process_input(true)

func processed_text(text):
	add_item(text)

func processed_jump(destination):
	add_item("Jumping to %s" % destination)

func processed_conditional(cond, next):
	add_item("Conditional (%s) to %s" % [cond, next])

func processed_ending():
	add_item("Story ends here")
	add_item("Current state is %s" % GBE.get_serialized_state())

func processed_option(text, dest, module):
	var option_number = current_options.size() + 1
	current_options[option_number] = [dest, module]
	if not module:
		module = "Current"
	add_item("%s (%s - %s): %s" % [option_number, dest, module, text])

func processed_character_speech(charname, text):
	add_item("%s says: %s" % [charname, text])

func processed_styled_text(style, text):
	add_item("[%s] %s" % [style, text])

func processed_media(media, parms):
	if "blur" in parms:
		add_item("{%s blurred} %s" % [media, parms])
	else:
		add_item("{%s} %s" % [media, parms])

func _input(ev):
	if ev is InputEventKey and not ev.echo and ev.pressed:
		if ev.scancode in optionkeys:
			for key in current_options.keys():
				if ev.scancode == optionkeys[key]:
					# Disable input until a section has been processed
					set_process_input(false)
					var selected = current_options[key][0]
					var module = current_options[key][1]
					current_options = {}
					add_item("----------------------------------------------------------------")
					if module:
						var path = "res://test/%s" % module
						add_item("Loading module %s" % path)
						GBE.load_twine_file(path)
					GBE.process_section(selected)
		elif ev.scancode == KEY_Q:
			get_tree().quit()
