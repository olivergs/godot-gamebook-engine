extends SceneTree

var current_options = {}

#func evaluate(expr):
#	# Initialize a GDScript
#	var script = GDScript.new()
#	script.set_source_code('# Context\n\nvar pepe = 2\nfunc eval():\n\treturn ' + expr)
#	script.reload()
#	# Initialize an object and set script for it
#	var obj = Reference.new()
#	obj.set_script(script)
#	# Execute our code and return the result
#	var data = obj.eval()
#	return data

func run_test(method):
	var status = "FAIL"
	if call(method):
		status = "PASS"
	print("%s %s" % [status, method])

func load_script(path):
	var script = load(path)
	return script.new()

func get_file_contents(path):
	var file = File.new()
	file.open(path, file.READ)
	var contents = file.get_as_text()
	file.close()
	return contents

func get_gbe():
	return load_script("res://scripts/gbe.gd")

func _init():
	run_test("test_load_story")
	run_test("test_process_section")
	
func test_load_story():
	print("Start")
	var GBE = get_gbe()
	GBE.load_story_file("res://test/test.gbe")
	assert(true)
	print("END")
	return true

func test_process_section():
	print("Start")
	var GBE = get_gbe()
	GBE.load_story_file("res://test/test.gbe")
#	GBE.connect("processed_text", self, "processed_text")
#	GBE.connect("processed_option", self, "processed_option")
	# Clean options
	GBE.process_section("@start")
	print("End")
	return true

# Signals
func processed_text(text):
	print(text)

func processed_option(text, dest):
	var option_number = current_options.size() + 1
	current_options[option_number] = dest
	print("%s: %s" % [option_number, text])
